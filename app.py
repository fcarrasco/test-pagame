from flask import Flask
from flask_jwt_simple import JWTManager

from inits import bcrypt, db, ma
from resources.api import api
from resources.backoffice import api as api_bo


def create_app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/pagarme.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'its a secret'
    app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
    app.config['JWT_AUTH_USERNAME_KEY'] = 'email'
    db.init_app(app)
    ma.init_app(app)
    bcrypt.init_app(app)
    jwt = JWTManager(app)
    api.init_app(app)
    api_bo.init_app(app)
    return app
