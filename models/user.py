from datetime import datetime

from sqlalchemy.orm import relationship

from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    validated = db.Column(db.Boolean(), nullable=False, default=False)
    balance = db.Column(db.Float(), nullable=False, default=0.0)
    created_at = db.Column(db.DateTime(), nullable=False,
                           default=datetime.utcnow)
    last_operation = db.Column(db.DateTime(), nullable=False,
                               default=datetime.utcnow)
    withdraws = relationship("Withdraw", back_populates="user")

    def __repr__(self):
        return '<User %r>' % self.name
