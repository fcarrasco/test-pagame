from datetime import datetime

from sqlalchemy.orm import relationship

from app import db


class Transfer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user_from = db.Column(db.Integer(), db.ForeignKey('user.id'),
                             nullable=False)
    id_user_to = db.Column(db.Integer(), db.ForeignKey('user.id'),
                           nullable=False)
    value = db.Column(db.Float(), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    refused = db.Column(db.Boolean(), nullable=False, default=False)
    refused_reason = db.Column(db.String(50))

    user_from = relationship("User", foreign_keys=[id_user_from])
    user_to = relationship("User", foreign_keys=[id_user_to])

    def __repr__(self):
        return '<Transfer %f from %s to %s>' % (self.value, self.user_from.name, self.user_to.name)
