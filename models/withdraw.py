from datetime import datetime

from sqlalchemy.orm import relationship

from app import db


class Withdraw(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer(), db.ForeignKey('user.id'), nullable=False)
    value = db.Column(db.Float(), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    refused = db.Column(db.Boolean(), nullable=False, default=False)
    refused_reason = db.Column(db.String(50))

    user = relationship("User", back_populates='withdraws')

    # def __repr__(self):
    #     return '<Withdraw %f user %s>' % (self.value, self.user.name)
