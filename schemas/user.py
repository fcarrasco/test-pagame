from marshmallow import validate
from marshmallow_sqlalchemy import field_for

from app import ma
from models.user import User


class UserSchema(ma.ModelSchema):
    id = field_for(User, 'id', dump_only=True)
    password = field_for(User, 'password', load_only=True, required=True)
    email = field_for(User, 'email', validate=validate.Email(error='Not a valid email address'))

    class Meta:
        model = User
        fields = ('name', 'email', 'password')
