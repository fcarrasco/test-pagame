from marshmallow_sqlalchemy import field_for

from app import ma
from models.withdraw import Withdraw


class WithdrawSchema(ma.ModelSchema):
    id = field_for(Withdraw, 'id', dump_only=True)

    class Meta:
        model = Withdraw
        fields = ('value', 'date')
