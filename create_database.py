from app import app, db

if __name__ == "__main__":
    print("Creating database tables...")
    db.create_all(app=app)
    print("Done!")
