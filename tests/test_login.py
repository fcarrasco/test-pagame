import json

import pytest

from app import bcrypt
from models.user import User


@pytest.fixture
def fixture_user(session):
    password = bcrypt.generate_password_hash('123456').decode('UTF-8')
    user = User(name='teste', password=password, email='felipe@test.com',
                validated=True)
    session.add(user)


def test_login_success(client, session, fixture_user):
    response = client.post(
        '/login',
        data=json.dumps(dict(
            email='felipe@test.com',
            password='123456'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert 'jwt' in data
    token = data['jwt']
    response = client.post(
        '/validate_token',
        headers={'Authorization': 'Bearer %s' % token},
        content_type='application/json'
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200


def test_login_fail(client, session, fixture_user):
    response = client.post(
        '/login',
        data=json.dumps(dict(
            email='felipe@test.com',
            password='wrong password'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 401


def test_login_wrong_params(client, session, fixture_user):
    response = client.post(
        '/login',
        data=json.dumps(dict(
            name='felipe@test.com',
            pwd='123456'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
