import json
from datetime import datetime, timedelta

import pytest
from flask_jwt_simple import create_jwt

from app import bcrypt
from models.transfer import Transfer
from models.user import User
from models.withdraw import Withdraw


@pytest.fixture
def fixture_backoffice_token():
    return create_jwt(identity='backoffice')

@pytest.fixture
def fixture_report1(session):
    password = bcrypt.generate_password_hash('123456').decode('UTF-8')
    user1 = User(name='user 1', password=password, email='user1@test.com',
                 validated=True, balance=500.0)
    session.add(user1)
    user2 = User(name='user 2', password=password, email='user2@test.com',
                 validated=True, balance=500.0)
    session.add(user2)
    date1 = datetime(2018, 2, 2, 0, 0, 0)
    transfer1 = Transfer(user_from=user1, user_to=user2, value=500,
                         refused=False, date=date1)
    session.add(transfer1)
    withdraw1 = Withdraw(user=user1, value=500, refused=False, date=date1)
    session.add(withdraw1)

    date2 = datetime(2018, 2, 1, 0, 0, 0)
    transfer2 = Transfer(user_from=user1, user_to=user2, value=800,
                         refused=False, date=date2)
    session.add(transfer2)
    withdraw2 = Withdraw(user=user1, value=800, refused=False, date=date2)
    session.add(withdraw2)

    date3 = datetime(2018, 1, 1, 0, 0, 0)
    transfer3 = Transfer(user_from=user1, user_to=user2, value=200,
                         refused=False, date=date3)
    session.add(transfer3)
    withdraw3 = Withdraw(user=user1, value=200, refused=False, date=date3)
    session.add(withdraw3)

    date4 = datetime(2017, 12, 12, 0, 0, 0)
    transfer4 = Transfer(user_from=user1, user_to=user2, value=1000,
                         refused=False, date=date4)
    session.add(transfer4)
    withdraw4 = Withdraw(user=user1, value=1000, refused=False, date=date4)
    session.add(withdraw4)

@pytest.fixture
def fixture_report2(session):
    password = bcrypt.generate_password_hash('123456').decode('UTF-8')
    date1 = datetime.utcnow() - timedelta(days=5)
    user1 = User(name='user 1', password=password, email='user1@test.com',
                 validated=True, balance=500.0, last_operation=date1)
    session.add(user1)
    date2 = datetime.utcnow() - timedelta(days=15)
    user2 = User(name='user 2', password=password, email='user2@test.com',
                 validated=True, balance=500.0, last_operation=date2)
    session.add(user2)
    date3 = datetime.utcnow() - timedelta(days=35)
    user3 = User(name='user 3', password=password, email='user3@test.com',
                 validated=True, balance=500.0, last_operation=date3)
    session.add(user3)
    date4 = datetime.utcnow() - timedelta(days=50)
    user4 = User(name='user 4', password=password, email='user4@test.com',
                 validated=True, balance=500.0, last_operation=date4)
    session.add(user4)


def test_report_operations_total_day(client, session, fixture_report1,
                                       fixture_backoffice_token):
    date = '2018-02-02'
    response = client.get(
        '/bo/reports/operations/day/%s' % date,
        content_type='application/json',
        headers={'Authorization': 'Bearer %s' % fixture_backoffice_token}
    )
    assert response.status_code == 200
    data = json.loads(response.data.decode())
    assert data['withdraws'] == 500.0
    assert data['transfers'] == 500.0


def test_report_operations_total_month(client, session, fixture_report1,
                                       fixture_backoffice_token):
    date = '2018-02'
    response = client.get(
        '/bo/reports/operations/month/%s' % date,
        content_type='application/json',
        headers={'Authorization': 'Bearer %s' % fixture_backoffice_token}
    )
    assert response.status_code == 200
    data = json.loads(response.data.decode())
    assert data['withdraws'] == 1300.0
    assert data['transfers'] == 1300.0


def test_report_operations_total_year(client, session, fixture_report1,
                                       fixture_backoffice_token):
    date = '2018'
    response = client.get(
        '/bo/reports/operations/year/%s' % date,
        content_type='application/json',
        headers={'Authorization': 'Bearer %s' % fixture_backoffice_token}
    )
    assert response.status_code == 200
    data = json.loads(response.data.decode())
    assert data['withdraws'] == 1500.0
    assert data['transfers'] == 1500.0


def test_report_operations_total(client, session, fixture_report1,
                                       fixture_backoffice_token):
    response = client.get(
        '/bo/reports/operations',
        content_type='application/json',
        headers={'Authorization': 'Bearer %s' % fixture_backoffice_token}
    )
    assert response.status_code == 200
    data = json.loads(response.data.decode())
    assert data['withdraws'] == 2500.0
    assert data['transfers'] == 2500.0


def test_report_inactive_users(client, session, fixture_report2,
                               fixture_backoffice_token):
    response = client.get(
        '/bo/reports/inactive_users',
        content_type='application/json',
        headers={'Authorization': 'Bearer %s' % fixture_backoffice_token}
    )
    assert response.status_code == 200
    data = json.loads(response.data.decode())
    assert data['inactive_users'] == 2
