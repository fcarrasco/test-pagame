import json

import pytest
from flask_jwt_simple import create_jwt

from app import bcrypt
from models.user import User


@pytest.fixture
def fixture_user1(session):
    password = bcrypt.generate_password_hash('123456').decode('UTF-8')
    user = User(name='teste', password=password, email='felipe@test.com',
                validated=True, balance=1000.0)
    session.add(user)
    session.flush()
    return user


@pytest.fixture
def fixture_user2(session):
    password = bcrypt.generate_password_hash('123456').decode('UTF-8')
    user = User(name='teste 2', password=password, email='felipe2@test.com',
                validated=True, balance=1000.0)
    session.add(user)
    session.flush()
    return user


@pytest.fixture
def fixture_token(fixture_user1):
    return create_jwt(identity=fixture_user1.id)


def test_withdraw_success(client, session, fixture_user1, fixture_user2,
                          fixture_token):
    response = client.post(
        '/transfer',
        data=json.dumps(dict(
            id_user=fixture_user2.id,
            value='500',
        )),
        headers={'Authorization': 'Bearer %s' % fixture_token},
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert fixture_user1.balance == 500.0
    assert fixture_user2.balance == 1500.0


def test_withdraw_without_funds(client, session, fixture_user1, fixture_user2,
                          fixture_token):
    response = client.post(
        '/transfer',
        data=json.dumps(dict(
            id_user=fixture_user2.id,
            value='1500',
        )),
        headers={'Authorization': 'Bearer %s' % fixture_token},
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert fixture_user1.balance == 1000.0
    assert fixture_user2.balance == 1000.0


def test_withdraw_bad_user(client, session, fixture_user1, fixture_user2,
                           fixture_token):
    response = client.post(
        '/transfer',
        data=json.dumps(dict(
            id_user=99,
            value='500',
        )),
        headers={'Authorization': 'Bearer %s' % fixture_token},
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
    assert fixture_user1.balance == 1000.0
    assert fixture_user2.balance == 1000.0
