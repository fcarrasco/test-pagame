import json


def test_register_success(client, session):
    response = client.post(
        '/register',
        data=json.dumps(dict(
            name='Felipe',
            email='felipe@test.com',
            password='123456'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert data['name'] == 'Felipe'
    assert data['email'] == 'felipe@test.com'


def test_register_invalid_email(client, session):
    response = client.post(
        '/register',
        data=json.dumps(dict(
            name='Felipe',
            email='not an email',
            password='123456'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 422


def test_register_without_password(client, session):
    response = client.post(
        '/register',
        data=json.dumps(dict(
            name='Felipe',
            email='not an email'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 422
