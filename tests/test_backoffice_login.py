import json


def test_login_backoffice_success(client, session):
    response = client.post(
        '/bo/login',
        data=json.dumps(dict(
            email='backoffice@test.com',
            password='backoffice'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200
    assert 'jwt' in data
    token = data['jwt']
    response = client.post(
        '/bo/validate_token',
        headers={'Authorization': 'Bearer %s' % token},
        content_type='application/json'
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 200


def test_login_fail(client, session):
    response = client.post(
        '/login',
        data=json.dumps(dict(
            email='backoffice@test.com',
            password='wrong password'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 401


def test_login_wrong_params(client, session):
    response = client.post(
        '/login',
        data=json.dumps(dict(
            name='felipe@test.com',
            pwd='123456'
        )),
        content_type='application/json',
    )
    data = json.loads(response.data.decode())
    assert response.status_code == 400
