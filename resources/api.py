from datetime import datetime

from flask import request
from flask_jwt_simple import create_jwt, decode_jwt, get_jwt_identity, jwt_required
from flask_restful import Api, Resource
from sqlalchemy.exc import IntegrityError

from app import bcrypt, db
from models.transfer import Transfer
from models.user import User
from models.withdraw import Withdraw
from schemas.user import UserSchema


class ApiResource(Resource):
    def get(self):
        return {'msg': 'pagarme test API'}


class RegisterResource(Resource):
    def post(self):
        json_data = request.get_json()
        if 'password' not in json_data or json_data['password'] == '':
            return {'msg': 'Invalid password'}, 422
        json_data['password'] = bcrypt.generate_password_hash(
            json_data['password']
        ).decode('UTF-8')
        schema = UserSchema()

        data = schema.load(json_data)
        if data.errors:
            return data.errors, 422
        user = data.data
        try:
            db.session.add(user)
            db.session.flush()
        except IntegrityError:
            return {'msg': 'user already registered'}, 400
        email_token = create_jwt(identity={'user_id': user.id,
                                           'email': user.email})
        print("VALIDATE URL: localhost:5000/validate_email/%s" % email_token)
        # TODO send validate email
        return schema.dump(user)


class LoginResource(Resource):
    def post(self):
        params = request.get_json()
        email = params.get('email', None)
        password = params.get('password', None)

        if not email:
            return {"msg": "Missing email parameter"}, 400
        if not password:
            return {"msg": "Missing password parameter"}, 400

        user = User.query.filter(User.email == email).first()
        if not user or not bcrypt.check_password_hash(user.password, password):
            return {"msg": "Bad username or password"}, 401
        if not user.validated:
            return {"msg": "User is not validated"}, 401
        ret = {'jwt': create_jwt(identity=user.id)}
        return ret, 200


class ValidateTokenResource(Resource):
    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        user = User.query.get(user_id)
        if not user:
            return {'msg', 'invalid token'}, 402
        return {'msg': 'valid token'}


class ValidateEmailResource(Resource):
    def get(self, token):
        data = decode_jwt(token)['sub']
        uid = data['user_id']
        email = data['email']
        user = User.query.get(uid)
        if not user or user.email != email:
            return {"msg": "Could not validate email"}, 400
        if user.validated:
            return {"msg": "User already validated"}, 400
        user.validated = True
        user.balance = 1000.0
        db.session.flush()
        return {"msg": "Validated"}, 200


class TransferResource(Resource):
    @jwt_required
    def post(self):
        user_id_from = get_jwt_identity()
        user_from = User.query.get(user_id_from)
        if not user_from:
            return {'msg', 'invalid token'}, 402
        json_data = request.get_json()
        value = float(json_data['value'])
        user_id_to = json_data['id_user']
        user_to = User.query.get(user_id_to)
        transfer = Transfer(user_from=user_from, user_to=user_to, value=value)
        if not user_to or not user_to.validated:
            return {'msg': 'Invalid user'}, 400
        if user_from.balance < value:
            transfer.refused = True
            transfer.refused_reason = 'Insuficient funds'
            return {'msg': 'Insuficient funds'}, 400
        if not transfer.refused:
            # TODO send mail
            user_from.balance = user_from.balance - value
            user_to.balance = user_to.balance + value
            user_from.last_operation = datetime.utcnow()
        db.session.add(transfer)
        db.session.flush()
        return {'msg': 'Transfer success'}


class WithdrawResource(Resource):
    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        user = User.query.get(user_id)
        if not user:
            return {'msg', 'invalid token'}, 402
        json_data = request.get_json()
        value = float(json_data['value'])
        withdraw = Withdraw(user=user, value=value)
        if user.balance < value:
            withdraw.refused = True
            withdraw.refused_reason = 'Insuficient funds'
            return {'msg': 'Insuficient funds'}, 400
        if not withdraw.refused:
            # TODO send mail
            user.balance = user.balance - value
            user.last_operation = datetime.utcnow()
        db.session.add(withdraw)
        db.session.flush()
        return {'msg': 'Withdraw success'}


api = Api()
api.add_resource(ApiResource, '/', endpoint='api')
api.add_resource(RegisterResource, '/register', endpoint='register')
api.add_resource(LoginResource, '/login', endpoint='login')
api.add_resource(ValidateTokenResource, '/validate_token',
                 endpoint='validate_token')
api.add_resource(ValidateEmailResource, '/validate_email/<token>',
                 endpoint='validate_email')
api.add_resource(TransferResource, '/transfer', endpoint='transfer')
api.add_resource(WithdrawResource, '/withdraw', endpoint='withdraw')
