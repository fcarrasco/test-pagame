import calendar
from datetime import datetime, timedelta

from flask import request
from flask_jwt_simple import create_jwt, get_jwt_identity, jwt_required
from flask_restful import Api, Resource
from sqlalchemy.sql import func

from app import db
from models.transfer import Transfer
from models.user import User
from models.withdraw import Withdraw


class BackofficeResource(Resource):
    def get(self):
        return {'msg': 'Backoffice pagarme test API'}


class LoginBackfficeResource(Resource):
    def post(self):
        params = request.get_json()
        email = params.get('email', None)
        password = params.get('password', None)

        if not email:
            return {"msg": "Missing email parameter"}, 400
        if not password:
            return {"msg": "Missing password parameter"}, 400

        if email != 'backoffice@test.com' or password != 'backoffice':
            return {"msg": "Bad username or password"}, 401
        ret = {'jwt': create_jwt(identity='backoffice')}
        return ret, 200


class ValidateBackofficeTokenResource(Resource):
    @jwt_required
    def post(self):
        user_id = get_jwt_identity()
        if user_id != 'backoffice':
            return {'msg': 'invalid token'}, 401
        return {'msg': 'valid token'}


class ReportOperationsResource(Resource):
    @jwt_required
    def get(self, interval=None, p=None):
        user_id = get_jwt_identity()
        if user_id != 'backoffice':
            return {'msg': 'invalid token'}, 401
        start_date = None
        end_date = None
        if interval == 'day':
            start_date = datetime.strptime(p, '%Y-%m-%d')
            start_date = start_date.replace(hour=0, minute=0, second=0)
            end_date = start_date + timedelta(hours=23, minutes=59, seconds=59)
        if interval == 'month':
            start_date = datetime.strptime(p, '%Y-%m')
            start_date = start_date.replace(day=1)
            end_date = start_date.replace(day=calendar.monthrange(
                start_date.year, start_date.month)[1])
            end_date = end_date.replace(hour=23, minute=59, second=59)
        if interval == 'year':
            start_date = datetime.strptime(p, '%Y')
            start_date = start_date.replace(day=1, month=1)
            end_date = start_date.replace(day=31, month=12)
            end_date = end_date.replace(hour=23, minute=59, second=59)
        query_withdraws = db.session.query(
            func.sum(Withdraw.value).label("total"))
        query_withdraws.filter(not Withdraw.refused)
        query_transfers = db.session.query(
            func.sum(Transfer.value).label("total"))
        query_transfers.filter(not Transfer.refused)
        if start_date and end_date:
            query_withdraws = query_withdraws.filter(
                Withdraw.date.between(start_date, end_date))
            query_transfers = query_transfers.filter(
                Transfer.date.between(start_date, end_date))
        total_withdraws = query_withdraws.one()[0]
        total_transfers = query_transfers.one()[0]
        return {
            'withdraws': total_withdraws,
            'transfers': total_transfers
        }


class ReportInactiveUsersResource(Resource):
    @jwt_required
    def get(self):
        user_id = get_jwt_identity()
        if user_id != 'backoffice':
            return {'msg': 'invalid token'}, 401
        d = datetime.utcnow() - timedelta(days=30)
        query = db.session.query(func.count(User.id).label("total"))
        query = query.filter(User.validated)
        query = query.filter(User.last_operation <= d)
        inactive_users = query.one()
        return {
            'inactive_users': inactive_users[0]
        }


api = Api()
api.add_resource(BackofficeResource, '/bo', endpoint='api_bo')
api.add_resource(LoginBackfficeResource, '/bo/login', endpoint='bo_login')
api.add_resource(ValidateBackofficeTokenResource, '/bo/validate_token',
                 endpoint='bo_validate_token')
api.add_resource(ReportOperationsResource, '/bo/reports/operations',
                 endpoint='bo_report_operations')
api.add_resource(ReportOperationsResource, '/bo/reports/operations/<interval>/<p>',
                 endpoint='bo_report_operations_filter')
api.add_resource(ReportInactiveUsersResource, '/bo/reports/inactive_users',
                 endpoint='bo_report_inactive_users')
